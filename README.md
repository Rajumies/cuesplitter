## Cuesplitter

---
Splits .flac- and .ape-files according to accompanied .cue-file using shnsplit.

Metadata is then tagged to split files using cuetag.

Cuesplitter is designed for mass editing, you can wrap it in a loop to split an entire library of files.

By default split files are output in a new folder inside the folder being worked on.

A different output directory can be specified with -o, which creates a folder <artist name - album name> (as specified in cue-sheet data) within the output directory.

---
Depends on:

**shntool**

**cuetools**

**flacon** see https://flacon.github.io/download/

Cuesplitter uses **enchanced getopt** from **util-linux** for commandline argument parsing.

---

## Examples


|Default mode        |Output specified    |
|:-------------------------:|:-------------------------:|
|![Default](desc/Default_example.png) | ![Output](desc/Output_example.png)|

**Special thanks to:**

Robert Siemer (https://stackoverflow.com/a/29754866)

on how to parse commandline arguments


Jorge (https://stackoverflow.com/a/51911626)

on neat way for multiline text formatting
