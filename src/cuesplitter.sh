#!/bin/bash

# Cuesplitter
# Author Janne Pisilä
# http://bitbucket.org/Rajumies/cuesplitter/

#TODO:
#1: Handle subdirectories, e.g. CD1 & CD2 - Still needs some checks to make sure files to split are found

# Fancy stuff to consider
#1: Output redirection so that errors would go to its own logfile

# Variable declarations, help and script description are in config file
source ./config.sh

# A primitive logrotater
loghandler ()
{
	if [ ! -f "$logfile" ]; then
		touch $logfile > /dev/null 2>&1
		if [ "$?" -ne 0 ]; then
			echo "Failed to create logfile: "$logfile""
			return 1
		fi
	fi
	# Log is rotated if it's more than 1000 lines long
	loglines=$(wc -l "$logfile" | cut -d ' ' -f 1)
    if [ "$loglines" -gt 1000 ]; then
    	# Store just one file for now
		mv "$logfile" "$logfile".1 && touch "$logfile"
		if [ "$?" -ne 0 ]; then
			echo "Failed to rotate logfile"
			return 1
		fi
    fi
    return 0
}

# Some initialization stuff before splitting
init ()
{
	log "---Starting a run in directory: $directory---"
	loghandler
	# Trimming possible trailing slashes from directory paths for consistency
	directory=$(echo "$directory" | sed 's:/*$::')
	output_dir=$(echo "$output_dir" | sed 's:/*$::')
	# Checks that correct directory is chosen, complains if there are more than 10 subdirectories
	# Non-interactive mode aborts
	dircheck=$(ls -ld "$directory"*/ | wc -l)
	if [ $dircheck -ge 10 ]; then
		log "There are $dircheck subdirectories in $directory"
		log "Cuesplitter is meant to be run in a directory containing media to split, and not e.g. in your media directory"
		if [ $interactive = 1 ]; then
			read -p "Are you sure you want to continue? (y/n): " selection
		else
			selection="n"
		fi
		if [[ $selection =~ ^[Nn]$ ]]; then
			log "Aborting"
			exit 0
		fi
	fi
	# If non-default output directory is specified, check and create it
	if [ "$default_output" = 0 ] && [ ! -d "$output_dir" ]; then
		mkdir "$output_dir" > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			log "$errorprefix Unable to create specified output directory $output_dir"
			log "Exiting..."
			exit 1
		fi
	fi

	log "Output directory set to: $output_dir"
}

show_help ()
{
	printf "$usage"
}

# Prints output to console and logs it prepending a timestamp
log ()
{
	echo "$1"
	printf "[%(%F %T)T] $1\n" >> $logfile
}

# Function which does the actual splitting and metadata tagging
split ()
{
	filetype="$1"
	# With default output attempt to create the output directory within the directory that's being worked on
	if [ "$default_output" = 1 ]; then
		mkdir "$directory"/"$output_dir" > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			log "$echoprefix$errorprefix Failed to create directory $directory/$output_dir"
			return 1
		else
			# Default value for output directory is 'output'. Prepend directory path to it so we can use just output_dir
			output_dir="$directory"/"$output_dir"
		fi
	# Otherwise create a <Artist - Album> directory in specified output directory
	else
		mkdir "$output_dir"/"$artist - $album" > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			log "$echoprefix$errorprefix Failed to create directory $output_dir/$artist - $album"
			return 1
		else
			# When output directory is specified, it already includes full path.
			# Append created <Artist - Album> directory to it
			output_dir="$output_dir"/"$artist - $album"
		fi
	fi
	# Shnsplit is run here
	# For now creates only .flac-files without any fancy options
	log "$echoprefix Splitting $filetype file found in $directory"
	shnsplit -f "$cuefile" -O always -t "%n - %t" -o flac -d "$output_dir" "$directory"/*."$filetype"
	split_status=$?
	if [ "$split_status" -ne 0 ]; then
		log "$echoprefix$errorprefix Failed to split file"
		return 1
	fi
	log "$echoprefix Success"
	# Sometimes the audio includes small sections with no media, which shnsplit happily creates
	# Remove them because otherwise the amount of files won't match the cue-sheet and cuetag will complain
	if [ -f "$output_dir"/*pregap* ]; then
		log "$echoprefix Removing pregap file"
		rm "$output_dir"/*pregap*
	fi
	# Cuetag is run here
	log "$echoprefix Performing metadata tagging"
	cuetag "$cuefile" "$output_dir"/*.flac
	tag_status=$?
	if [ "$tag_status" -ne 0 ]; then
		log "$echoprefix$errorprefix Failed to tag metadata"
		return 1
	fi
	log "$echoprefix Success"
	return 0
}
##### End of functions #####

##### Start of execution #####

##### Commandline argument parsing start #####
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    exit 1
fi

! PARSED=$(getopt --options=$opts --longoptions=$longopts --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
	exit 2
fi

eval set -- "$PARSED"

while true; do
	case "$1" in
	-"${opts:0:1}"|--"${lopts[0]%?}")
		#Search directory
		directory="$2"
		shift 2
	;;
	-"${opts:2:1}"|--"${lopts[1]%?}")
		#Output directory
		output_dir="$2"
		default_output=0
		shift 2
	;;
	-"${opts:4:1}"|--"${lopts[2]}")
		#Help
		show_help
		printf "$desc"
		exit 0
	;;
	-"${opts:5:1}"|--"${lopts[3]}")
		#Non-interactive mode
		interactive=0
		shift
	;;
	-"${opts:6:1}"|--"${lopts[4]}")
		#Version information
		printf "$vers"
		exit 0
	;;
	--)
		shift
		break
	;;
	*|?)
		exit 1
	;;
	esac
done

##### Commandline argument parsing end #####

init

# Finds all *.cue files in directory and all subdirectories
# readarray with triple less than reads find results into an array
readarray -t cuefile <<< "$(find "$directory" -name *.cue -type f)"

# More than one .cue file was found
if [ "${#cuefile[@]}" -gt 1 ]; then
	log "Found more than one .cue-file in $directory:"
	# Prints all .cue files while stripping directory path for readability
	# Some arithmetics are requires since arrays start from 0
	# but it's clearer for selection to start from 1
	for i in "${!cuefile[@]}"
	do
		arrayfile=$(echo "${cuefile[$i]}" | awk -F"$directory" '{print $NF}')
		log "$(( $i + 1 )): $arrayfile"
	done
	# Infinite loop until we get what we want from user
	# Non-interactive mode skips
	while true; do
		if [ $interactive -eq 1 ]; then
			read -p "Please choose a file to use [1], ..., [n], [s]kip: " selection
		else
			selection="s"
		fi
		if [[ $selection =~ ^[Ss]$ ]]; then
			log "Skipping..."
			exit 0
		elif [ $selection -ge 1 ] && [ $selection -le ${#cuefile[@]} ]; then
			cuefile=${cuefile[$(( $selection - 1 ))]}
			log "Selected: $cuefile"
			break
		else
			echo "Invalid choice"
		fi
	done
# If there is only one .cue-file, no actions are necessary
elif [ "${#cuefile[@]}" -eq 1 ]; then
	log "Found .cue-file in $directory"
else
	log "No .cue-files in $directory"
	exit 0
fi

# filetype: filetype cue-sheet points to (flac|ape). Doesn't include leading point
# artist: Artist as specified in cue-sheet by PERFORMER-field
# album: Album as specified in cue-sheet by TITLE-field
filetype=$(awk -F'[."]' '$1=="FILE "{print $3}' "$cuefile")
artist=$(awk -F\" '$1=="PERFORMER "{print $2; exit;}' "$cuefile")
album=$(awk -F\" '$1=="TITLE "{print $2; exit;}' "$cuefile")
log "$echoprefix Associated filetype is .$filetype"

# For now handle flac and ape, otherwise we do nothing
case "$filetype" in
"flac")
;;
"ape")
;;
*)
	log "$echoprefix Unknown filetype $filetype"
	exit 1
;;
esac
# Split function call here
split "$filetype"
STATUS=$?
if [ "$STATUS" = 0 ]; then
	log "$echoprefix $directory was split and tagged successfully"
else
	log "$echoprefix $directory failed"
fi

